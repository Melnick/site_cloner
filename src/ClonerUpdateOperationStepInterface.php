<?php

namespace Drupal\site_cloner;

/**
 * Interface for cloner_operation_step plugins.
 *
 * Used for plugins that must update the data after editing a site entity.
 */
interface ClonerUpdateOperationStepInterface {

  public function getUpdateSteps();

  public function processUpdateStep($step);

}
