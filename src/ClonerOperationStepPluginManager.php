<?php

namespace Drupal\site_cloner;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * ClonerOperationStep plugin manager.
 */
class ClonerOperationStepPluginManager extends DefaultPluginManager {

  /**
   * Constructs ClonerOperationStepPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ClonerOperationStep',
      $namespaces,
      $module_handler,
      'Drupal\site_cloner\ClonerOperationStepInterface',
      'Drupal\site_cloner\Annotation\ClonerOperationStep'
    );
    $this->alterInfo('cloner_operation_step_info');
    $this->setCacheBackend($cache_backend, 'cloner_operation_step_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getOperationDefinitions($operation = 'install') {
    $definitions = $this->getDefinitions();

    if ($operation !== 'install') {

      $operation_interface = $this->getOperationInterface($operation);

      $definitions = array_filter(
        $definitions,
        function ($definition, $id) use ($operation_interface) {
          return is_subclass_of($definition['class'], $operation_interface);
        },
        ARRAY_FILTER_USE_BOTH);
    }

    uasort($definitions, [
      'Drupal\Component\Utility\SortArray',
      'sortByWeightElement',
    ]);

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperationSteps($site_id, $operation = 'install') {
    $definitions = $this->getOperationDefinitions($operation);
    $steps = [];

    $operation_steps_method = 'get' . ucfirst($operation) . 'Steps';

    foreach ($definitions as $step_id => $definition) {
      $step_instance = $this->createInstance($step_id, [
        'site_id' => $site_id,
      ]);

      $definition_steps = call_user_func([$step_instance, $operation_steps_method]);

      foreach ($definition_steps as $step_data) {
        $steps[] = ['id' => $step_id, 'data' => $step_data];
      }
    }

    return $steps;
  }

  /**
   * {@inheritdoc}
   */
  public function processStep($step, $site_id, $operation) {
    call_user_func([
      $this->createInstance($step['id'], ['site_id' => $site_id]),
      'process' . ucfirst($operation) . 'Step',
    ],
    $step['data']
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getOperationInterface($operation) {
    return 'Drupal\site_cloner\Cloner' . ucfirst($operation) . 'OperationStepInterface';
  }

}
