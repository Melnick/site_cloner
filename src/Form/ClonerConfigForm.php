<?php

namespace Drupal\site_cloner\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Basic form for configuration site cloner.
 */
class ClonerConfigForm extends ConfigFormBase {

  /**
   * Config name.
   *
   * @var string
   */
  const CONFIG_NAME = 'site_cloner.settings';

  /**
   * Editable config to store site cloner settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function __construct($multisite_config_entity_storage) {
    parent::__construct($multisite_config_entity_storage);
    $this->settings = $this->config(self::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_cloner.config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config = NULL) {

    $form['models'] = [
      '#type' => 'element_multiple',
      '#title' => $this->t('Models'),
      '#header' => [
        ['data' => $this->t('ID'), 'width' => '20%'],
        ['data' => $this->t('Name'), 'width' => '30%'],
        ['data' => $this->t('Domain'), 'width' => '30%'],
      ],
      '#element' => [
        'id' => [
          // @todo We need select there.
          '#type' => 'textfield',
        ],
        'name' => [
          '#type' => 'textfield',
        ],
        'domain' => [
          // @todo We need validation there.
          '#type' => 'textfield',
        ],
      ],
      '#default_value' => $this->settings->get('models'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->settings->set('models', $form_state->getValue('models'))->save();
  }

}
