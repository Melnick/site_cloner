<?php

namespace Drupal\site_cloner\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\site_cloner\Services\ClonerHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for Select Site Model Form.
 */
class SelectSiteModel extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected $clonerHelperService;

  /**
   * {@inheritdoc}
   */
  public function __construct(ClonerHelper $cloner_helper_service) {
    $this->clonerHelperService = $cloner_helper_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('site_cloner.helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'select_site_model';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $multisite = NULL) {

    $models_options = $this->clonerHelperService->getModelsOptionList();

    if (!$models_options) {
      $form['empty']['#markup'] = $this->t('No models available. Contact the site administrator.');
    }

    $form['model'] = [
      '#type' => 'select',
      '#options' => $models_options,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this
        ->t('Create new site'),
      "#attributes" => ["class" => ['button--primary']],
    ];

    // @todo We need validation here that ensures that the selected model actually exists and is configured.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.site.add_form', ['model_id' => $form_state->getValue('model')]);
  }

}
