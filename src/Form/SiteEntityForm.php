<?php

namespace Drupal\site_cloner\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Site entity form.
 *
 * @property \Drupal\site_cloner\SiteInterface $entity
 */
class SiteEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Name for the site.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\site_cloner\Entity\Site::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->get('domain'),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $operation = $this->entity->isNew() ? 'install' : 'update';
    $result = parent::save($form, $form_state);

    $form_state->setRedirect('site_cloner.batch', [
      'site_id' => $this->entity->id(),
      'operation' => $operation,
    ]);

    return $result;
  }

}
