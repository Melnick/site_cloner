<?php

namespace Drupal\site_cloner;

/**
 * Interface for cloner_operation_step plugins.
 */
interface ClonerOperationStepInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  public function getInstallSteps();

  public function processInstallStep($step);

}
