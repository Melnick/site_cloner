<?php

namespace Drupal\site_cloner\Services;

use Drupal\site_cloner\Form\ClonerConfigForm;

/**
 * Cloner helper service.
 */
class ClonerHelper {

  /**
   * {@inheritdoc}
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected $models;

  /**
   * {@inheritdoc}
   */
  protected $modelsOptionList;

  /**
   * {@inheritdoc}
   */
  protected $settings;


  /**
   * {@inheritdoc}
   */
  public $sitesDir;

  /**
   * {@inheritdoc}
   */
  public function __construct($config_factory) {
    $this->configFactory = $config_factory;
    $this->settings = $this->configFactory->get(ClonerConfigForm::CONFIG_NAME);

    if (!empty($this->settings) && !empty($this->settings->get('models'))) {
      foreach ($this->settings->get('models') as $model) {
        $this->models[$model['id']] = $model;
        $this->modelsOptionList[$model['id']] = $model['name'];
      }
    }

    $this->sitesDir = DRUPAL_ROOT . "/sites";
  }

  /**
   * {@inheritdoc}
   */
  public function getModelsOptionList() {
    return $this->modelsOptionList ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getModels() {
    return $this->models ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteDir($id) {
    return $this->sitesDir . "/" . $id;
  }

}
