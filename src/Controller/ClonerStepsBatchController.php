<?php

namespace Drupal\site_cloner\Controller;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Batch Controller.
 *
 * Used for batch execution of site cloner operations.
 * Can be extended by plugins @ClonerOperationStep.
 *
 * @see src/ClonerOperationStepPluginManager.php
 */
class ClonerStepsBatchController extends ControllerBase {
  use DependencySerializationTrait;

  /**
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * @var \Drupal\site_cloner\ClonerOperationStepPluginManager
   */
  protected $pluginManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $siteStorage;

  /**
   * @var \Drupal\site_cloner\Entity\Site
   */
  protected $site;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_manager_cloner_operation_step, $site_storage, $logger) {
    $this->batchBuilder = new BatchBuilder();
    $this->pluginManager = $plugin_manager_cloner_operation_step;
    $this->siteStorage = $site_storage;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.cloner_operation_step'),
      $container->get('entity_type.manager')->getStorage('site'),
      $container->get('logger.factory')->get('site_cloner'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runBatch($site_id, $operation = 'install') {

    $this->batchBuilder
      ->setTitle($this->t('Processing'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));

    $this->site = $this->siteStorage->load($site_id);

    if (!$this->site) {
      throw new NotFoundHttpException();
    }

    $steps = $this->pluginManager->getOperationSteps($site_id, $operation);

    if (empty($steps)) {
      throw new NotFoundHttpException();
    }

    $this->batchBuilder->addOperation([$this, 'processItems'], [$steps, $site_id, $operation]);
    $this->batchBuilder->setFinishCallback([$this, 'finished']);
    $bath = $this->batchBuilder->toArray();

    batch_set($bath);
    return batch_process();
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, $site_id, $operation, array &$context) {
    // Elements per operation.
    $limit = 5;

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $this->processItem($item, $site_id, $operation);
          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = $this->t('Now processing step :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item, $site_id, $operation) {
    try {
      $this->pluginManager->processStep($item, $site_id, $operation);
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }
  }

  /**
   * Finished callback for batch.
   */
  public function finished($success, $results, $operations) {
    // @todo We need add logic for uninstall operation.
    return new RedirectResponse(Url::fromRoute('entity.site.collection')->toString());
  }

}
