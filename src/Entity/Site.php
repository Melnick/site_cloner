<?php

namespace Drupal\site_cloner\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\site_cloner\SiteInterface;

/**
 * Defines the site entity type.
 *
 * @ConfigEntityType(
 *   id = "site",
 *   label = @Translation("Site"),
 *   label_collection = @Translation("Sites"),
 *   label_singular = @Translation("site"),
 *   label_plural = @Translation("sites"),
 *   label_count = @PluralTranslation(
 *     singular = "@count site",
 *     plural = "@count sites",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\site_cloner\SiteListBuilder",
 *     "form" = {
 *       "add" = "Drupal\site_cloner\Form\SiteEntityForm",
 *       "edit" = "Drupal\site_cloner\Form\SiteEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "site",
 *   admin_permission = "administer site",
 *   links = {
 *     "collection" = "/admin/structure/site",
 *     "add-form" = "/admin/structure/site/add",
 *     "edit-form" = "/admin/structure/site/{site}",
 *     "delete-form" = "/admin/structure/site/{site}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description"
 *   }
 * )
 */
class Site extends ConfigEntityBase implements SiteInterface {

  /**
   * The site ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The site label.
   *
   * @var string
   */
  protected $label;

  /**
   * The site status.
   *
   * @var bool
   */
  protected $status;

  /**
   * ID of the model on which the site is based.
   *
   * @var string
   */
  protected $model;

  /**
   * The site domain.
   *
   * @var string
   */
  protected $domain;

}
