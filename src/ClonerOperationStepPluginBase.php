<?php

namespace Drupal\site_cloner;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for cloner_operation_step plugins.
 */
abstract class ClonerOperationStepPluginBase extends PluginBase implements ClonerOperationStepInterface, ContainerFactoryPluginInterface {

  protected $entityTypeManager;

  protected $site;
  protected $clonerHelperService;

  /**
   * {@inheritdoc}
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $entity_type_manager, $site_cloner_helper, $database, $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->site = $this->entityTypeManager->getStorage('site')->load($configuration['site_id']);
    $this->clonerHelperService = $site_cloner_helper;
    $this->database = $database;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('site_cloner.helper'),
      $container->get('database'),
      $container->get('file_system'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getInstallSteps() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function processInstallStep($step) {
    return [];
  }

}
