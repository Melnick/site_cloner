<?php

namespace Drupal\site_cloner\Plugin\ClonerOperationStep;

use Drupal\site_cloner\ClonerUninstallOperationStepInterface;
use Drupal\site_cloner\Plugin\ClonerOperationStep\Base\PrepareDirectory;

/**
 * Plugin used for create "sites/{site_id}" multisite  directory.
 *
 * @ClonerOperationStep(
 *   id = "create_site_dir",
 *   label = @Translation("Create site dirrectory"),
 *   description = "",
 *   weight = -100
 * )
 */
class CreateSiteDir extends PrepareDirectory implements ClonerUninstallOperationStepInterface {

  /**
   * {@inheritdoc}
   */
  public function getInstallSteps() {
    return [
      $this->clonerHelperService->getSiteDir($this->site->id()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getUninstallSteps() {
    return $this->getInstallSteps();
  }

}
