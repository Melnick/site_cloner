<?php

namespace Drupal\site_cloner\Plugin\ClonerOperationStep\Base;

use Drupal\Core\File\FileSystemInterface;
use Drupal\site_cloner\ClonerOperationStepPluginBase;

/**
 * Base @ClonerOperationStep plugin class, for plugins that create directories.
 */
class PrepareDirectory extends ClonerOperationStepPluginBase {

  /**
   * {@inheritdoc}
   */
  public function processInstallStep($step) {
    $this->fileSystem->prepareDirectory($step, FileSystemInterface::CREATE_DIRECTORY);
  }

  /**
   * {@inheritdoc}
   */
  public function processUninstallStep($step) {
    $this->fileSystem->deleteRecursive($step);
  }

}
