<?php

namespace Drupal\site_cloner;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a site entity type.
 */
interface SiteInterface extends ConfigEntityInterface {

}
