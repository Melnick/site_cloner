<?php

namespace Drupal\site_cloner\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines cloner_operation_step annotation object.
 *
 * @Annotation
 */
class ClonerOperationStep extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The weight of the plugin.
   * Determines the order in which the steps are performed.
   *
   * @var int
   */
  public $weight = 0;

}
