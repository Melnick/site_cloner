<?php

namespace Drupal\site_cloner;

/**
 * Interface for cloner_operation_step plugins.
 *
 * Used for plugins which should delete the data, once after site entity deleting.
 */
interface ClonerUninstallOperationStepInterface {

  public function getUninstallSteps();

  public function processUninstallStep($step);

}
